import { shallowMount } from '@vue/test-utils';
import FactShow from '@/components/FactShow.vue';

describe('FactSwow.vue', () => {
  it('renders a factorial', async () => {
    const wrapper = shallowMount(FactShow);
    expect(wrapper.element).toMatchSnapshot('initial');
    await wrapper.vm.$nextTick();
    expect(wrapper.element).toMatchSnapshot('loaded');
  });
});
