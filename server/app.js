const express = require('express');

function configureRoutes(app) {
  const rootRouter = require('./routes/root.route');
  app.use('/', rootRouter);
}

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

configureRoutes(app);

module.exports = app;
