const express = require('express');
const factLib = require('js-fact-lib');

const router = express.Router();

router.get('/', (_req, res) => {
  res.send({
    fact_of_3: factLib(3),
  });
});

module.exports = router;
